myRow=1; myT=new Array(); myT1=new Array(); myT2=new Array();
myOK=new Array(); myOK1=new Array();myOK2=new Array();
 
function initTable() {
   document.dataTable.setAutoRefresh(false);            
   document.dataTable.setDefault();
   document.dataTable.setSeriesLabel(1,"t(min)");
   document.dataTable.setSeriesLabel(2,"�1(�C)");
   document.dataTable.setSeriesLabel(3,"�2(�C)");
   document.dataTable.setAutoRefresh(true); 
   table = document.dataTable.getTableID();
}

function initGraph() {
   document.dataGraph.setAutoRefresh(false);
   document.dataGraph.setDefault();
   document.dataGraph.setSeriesStyle(1,false,2);
   document.dataGraph.setSeriesRGB(1,255,0,0);
   document.dataGraph.setSeriesStyle(2,false,2);
   document.dataGraph.setSeriesRGB(2,0,0,255);
   document.dataGraph.setLabelX("t(min)");
   document.dataGraph.setLabelY("�(�C)");
   document.dataGraph.setAutoRefresh(true);
   graph = document.dataGraph.getGraphID();
}

function rowUp() {
   if (myRow>1) { myRow=myRow-1; }
   if (myOK[myRow]=="OK")  T=myT[myRow]; else T="";
   if (myOK1[myRow]=="OK") T1=myT1[myRow]; else T1="";
   if (myOK2[myRow]=="OK") T2=myT2[myRow]; else T2="";
   
   document.myForm.TXT_T.value=T
   document.myForm.TXT_A.value=T1
   document.myForm.TXT_B.value=T2
   document.dataTable.setActiveCell(myRow,0);
}

function rowDwn() {
   if (myRow<15) { myRow=myRow+1; }
   if (myOK[myRow]=="OK")  T=myT[myRow]; else T="";
   if (myOK1[myRow]=="OK") T1=myT1[myRow]; else T1="";
   if (myOK2[myRow]=="OK") T2=myT2[myRow]; else T2="";
   
   document.myForm.TXT_T.value=T
   document.myForm.TXT_A.value=T1
   document.myForm.TXT_B.value=T2
   document.dataTable.setActiveCell(myRow,0);
}

function rowOK() {
   Tmp=myForm.TXT_T.value;
   if (!Tmp=="") { myT[myRow]=eval(Tmp);
   		  document.dataTable.setActiveCell(myRow,1);
		  document.dataTable.setActiveCellValue(myT[myRow]); myOK[myRow]="OK";
   Tmp=myForm.TXT_A.value;
   if (!Tmp=="") { myT1[myRow]=eval(Tmp);
   		  document.dataTable.setActiveCell(myRow,2);
		  document.dataTable.setActiveCellValue(myT1[myRow]); myOK1[myRow]="OK";}
   Tmp=myForm.TXT_B.value;
   if (!Tmp=="") { myT2[myRow]=eval(Tmp);
   		  document.dataTable.setActiveCell(myRow,3);
		  document.dataTable.setActiveCellValue(myT2[myRow]); myOK2[myRow]="OK";}
   rowDwn(); }

   document.dataGraph.clearAllSeries();
   for (i=1; i<16; i++) { if (myOK1[i]=="OK") document.dataGraph.addDatum(1,myT[i],myT1[i]); 
                          if (myOK2[i]=="OK") document.dataGraph.addDatum(2,myT[i],myT2[i]); }

}