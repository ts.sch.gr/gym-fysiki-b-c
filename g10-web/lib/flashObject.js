/*

*/

if (typeof(Flash) == "undefined") {
    Flash = {};
}

if (typeof(Flash.FlashObject) == "undefined") {
    Flash.FlashObject = {};
}

Flash.FlashObject.NAME = "Flash.FlashObject";
Flash.FlashObject.VERSION = "1.32";
Flash.FlashObject.__repr__ = function () {
    return "[" + this.NAME + " " + this.VERSION + "]";
};
Flash.FlashObject.toString = function () {
    return this.__repr__();
};
Flash.FlashObject.EXPORT = ['FlashObject'];
Flash.FlashObject.EXPORT_OK = [];
Flash.FlashObject.EXPORT_TAGS = {
    ':all': Flash.FlashObject.EXPORT,
    ':common': Flash.FlashObject.EXPORT
};

/*

*/

Flash.FlashObject.FlashObject = function (swf, id, w, h, ver, c, quality, redirectUrl, detectKey) {
    if (!detectKey) {
        detectKey = 'detectflash';
    }
    if (!quality) {
        quality = 'high';
    }
    this.DETECT_KEY = detectKey;
    this.skipDetect = Flash.FlashObject.getRequestParameter(this.DETECT_KEY);
    this.params = {};
    this.variables = {};
    this.attributes = [];

    if (swf) {
        this.setAttribute('swf', swf);
    }
    if (id) {
        this.setAttribute('id', id);
    }
    if (w) {
        this.setAttribute('width', w);
    }
    if (h) {
        this.setAttribute('height', h);
    }
    if (ver) {
        this.setAttribute('version', new Flash.FlashObject.PlayerVersion(ver.toString().split(".")));
    }
    if (c) {
        this.addParam('bgcolor', c);
    }
    this.addParam('quality', quality);
    this.setAttribute('redirectUrl', '');
    if (redirectUrl) {
        this.setAttribute('redirectUrl', redirectUrl);
    }
};

Flash.FlashObject.FlashObject.prototype.setAttribute = function (name, value) {
    this.attributes[name] = value;
};

Flash.FlashObject.FlashObject.prototype.getAttribute = function (name) {
    return this.attributes[name];
};

Flash.FlashObject.FlashObject.prototype.getAttributes = function () {
    return this.attributes;
};

Flash.FlashObject.FlashObject.prototype.addParam = function (name, value) {
    this.params[name] = value;
};

Flash.FlashObject.FlashObject.prototype.getParams = function () {
    return this.params;
};

Flash.FlashObject.FlashObject.prototype.getParam = function (name) {
    return this.params[name];
};

/*

*/
Flash.FlashObject.FlashObject.prototype.addVariable = function (name, value) {
    this.variables[name] = value;
};

Flash.FlashObject.FlashObject.prototype.getVariable = function (name) {
    return this.variables[name];
};

Flash.FlashObject.FlashObject.prototype.getVariables = function () {
    return this.variables;
};

Flash.FlashObject.FlashObject.prototype.getParamTags = function () {
   var paramTags = [];
   var params = this.getParams();
   for (var key in params) {
        paramTags.push('<param name="' + key + '" value="' + params[key] + '" />');
   }
   return paramTags.join('');
};

Flash.FlashObject.FlashObject.prototype.getVariablePairs = function () {
    var variablePairs = [];
    var variables = this.getVariables();
    for (var key in variables) {
        // XXX: potential issues with escape / encodeURIComponent?
        var value = variables[key];
        variablePairs.push(key + "=" + escape(value));
    }
    return variablePairs;
};

Flash.FlashObject.FlashObject.prototype.getHTML = function () {
    var flashHTML = [];
    if (navigator.plugins && navigator.mimeTypes && navigator.mimeTypes.length) {
        // netscape plugin architecture
        flashHTML.push(
            '<embed type="application/x-shockwave-flash"' +
            ' src="' + this.getAttribute('swf') +
            '" width="' + this.getAttribute('width') +
            '" height="'+ this.getAttribute('height') +
            '" id="'+ this.getAttribute('id') +
            '" name="'+ this.getAttribute('id') +
            '"'
        )
        var params = this.getParams();
        for (var key in params) {
            flashHTML.push(' ' + key + '="' + params[key] +'"');
        }
        pairs = this.getVariablePairs().join("&");
        if (pairs.length > 0) {
            flashHTML.push(' flashvars="' + pairs + '"');
        }
        flashHTML.push('></embed>');
    } else {
        // PC IE
        flashHTML.push(
            '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"' +
            ' width="' + this.getAttribute('width') +
            '" height="' + this.getAttribute('height') +
            '" id="' + this.getAttribute('id') + '">'
        );
        flashHTML.push(
            '<param name="movie" value="' +
            this.getAttribute('swf') +
            '" />'
        );
        flashHTML.push(this.getParamTags());
        var pairs = this.getVariablePairs().join("&");
        if (pairs.length > 0) {
            flashHTML.push('<param name="flashvars" value="' + pairs + '" />');
        }
        flashHTML.push('</object>');
    }
    return flashHTML.join('');
};

/*

*/

Flash.FlashObject.FlashObject.prototype.write = function (elementId) {
    var version = Flash.FlashObject.getPlayerVersion();
    if (this.skipDetect || (version && version.versionIsValid(this.getAttribute('version')))) {
        if (document.getElementById) {
            document.getElementById(elementId).innerHTML = this.getHTML();
        }
    } else {
        if (this.getAttribute('redirectUrl') != "") {
            document.location.replace(this.getAttribute('redirectUrl'));
        }
    }
};

/*

*/

/* ---- detection functions ---- */
Flash.FlashObject.getPlayerVersion = function () {
    var playerVersion;
    if (navigator.plugins && navigator.mimeTypes.length){
        var x = navigator.plugins["Shockwave Flash"];
        if (x && x.description) {
            playerVersion = new Flash.FlashObject.PlayerVersion(x.description.replace(/([a-z]|[A-Z]|\s)+/, "").replace(/(\s+r|\s+b[0-9]+)/, ".").split("."));
        }
    } else {
        try {
            var axo = new ActiveXObject("ShockwaveFlash.ShockwaveFlash");
            playerVersion = new Flash.FlashObject.PlayerVersion(axo.GetVariable("$version").split(" ")[1].split(",")); 
        } catch (e) {
            // pass
        }
    }
    return playerVersion;
};

Flash.FlashObject.PlayerVersion = function (arrVersion) {
    this.major = parseInt(arrVersion[0], 10) || 0;
    this.minor = parseInt(arrVersion[1], 10) || 0;
    this.rev = parseInt(arrVersion[2], 10) || 0;
};

Flash.FlashObject.PlayerVersion.prototype.versionIsValid = function (fv) {
    if (!fv) {
        return false;
    }
    if (this.major < fv.major) {
        return false;
    }
    if (this.major > fv.major) {
        return true;
    }
    if (this.minor < fv.minor) {
        return false;
    }
    if (this.minor > fv.minor) {
        return true;
    }
    if (this.rev < fv.rev) {
        return false;
    }
    return true;
};

/* ---- get value of query string param ---- */
Flash.FlashObject.getRequestParameter = function (param) {
    var q = document.location.search || document.location.href.hash;
    if (q) {
        var startIndex = q.indexOf(param +"=");
        var endIndex = (q.indexOf("&", startIndex) > -1) ? q.indexOf("&", startIndex) : q.length;
        if (q.length > 1 && startIndex > -1) {
            return q.substring(q.indexOf("=", startIndex) + 1, endIndex);
        }
    }
    return "";
};

if (typeof(JSAN) == 'undefined') {
    (function () {
        var self = Flash.FlashObject;
        var exports = self.EXPORT;
        for (var i = 0; i < exports.length; i++) {
            var name = exports[i];
            this[name] = self[name];
        }
    })();
}

/*

*/

/*

*/
